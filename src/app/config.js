'use strict'

let parameters = {}

if (process.env.NODE_ENV === 'habitat') {
  try {
    parameters = require('./../parameters.json')
  } catch (err) {
    // TODO empty errors
  }
} else {
  parameters = require('./../parameters.json')
}

module.exports = parameters
