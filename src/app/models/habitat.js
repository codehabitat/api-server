'use strict'

const mongoose = require('mongoose')
const mongoClient = require('./../fn/mongooseClient')
const { composeWithMongoose } = require('graphql-compose-mongoose')

const ComponentSchema = new mongoose.Schema({
  name: { type: String },
  image: { type: String },
  registry: { type: String },
  tag: { type: String },
  public: { type: Boolean },
  subdomain: { type: String },
})

const HabitatSchema = new mongoose.Schema({
  name: { type: String },
  projectId: { type: String },
  components: [ComponentSchema],
  status: { type: String },
})

const HabitatModel = mongoClient.model('Habitat', HabitatSchema)

// STEP 2: CONVERT MONGOOSE MODEL TO GraphQL PIECES
const customizationOptions = {} // left it empty for simplicity, described below
const HabitatTC = composeWithMongoose(HabitatModel, customizationOptions)

module.exports = HabitatTC
