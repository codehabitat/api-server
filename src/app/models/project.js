'use strict'

const mongoose = require('mongoose')
const mongoClient = require('./../fn/mongooseClient')
const { composeWithMongoose } = require('graphql-compose-mongoose')

const ComponentSchema = new mongoose.Schema({
    name: { type: String },
    image: { type: String },
    registry: { type: String },
    children: [String]
})

const ProjectSchema = new mongoose.Schema({
    name: { type: String },
    slug: { type: String },
    components: [ComponentSchema],
})

const ProjectModel = mongoClient.model('Project', ProjectSchema)

// STEP 2: CONVERT MONGOOSE MODEL TO GraphQL PIECES
const customizationOptions = {} // left it empty for simplicity, described below
const ProjectTC = composeWithMongoose(ProjectModel, customizationOptions)

module.exports = ProjectTC
