'use strict'

const mongoose = require('mongoose')
const mongoClient = require('./../fn/mongooseClient')

const UserSchema = new mongoose.Schema({
  googleId: { type: String },
})

const UserModel = mongoClient.model('User', UserSchema)

module.exports = UserModel
