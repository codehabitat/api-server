'use strict'

const { GQC } = require('graphql-compose')

const ProjectTC = require('./project')
const HabitatTC = require('./habitat')

GQC.rootQuery().addFields({
  // Project
  projectById: ProjectTC.getResolver('findById'),
  projectByIds: ProjectTC.getResolver('findByIds'),
  projectOne: ProjectTC.getResolver('findOne'),
  projectMany: ProjectTC.getResolver('findMany'),
  projectCount: ProjectTC.getResolver('count'),
  projectConnection: ProjectTC.getResolver('connection'),
  projectPagination: ProjectTC.getResolver('pagination'),
  // Habitat
  habitatMany: HabitatTC.getResolver('findMany'),
})

GQC.rootMutation().addFields({
  // Project
  projectCreate: ProjectTC.getResolver('createOne'),
  projectUpdateById: ProjectTC.getResolver('updateById'),
  projectUpdateOne: ProjectTC.getResolver('updateOne'),
  projectUpdateMany: ProjectTC.getResolver('updateMany'),
  projectRemoveById: ProjectTC.getResolver('removeById'),
  projectRemoveOne: ProjectTC.getResolver('removeOne'),
  projectRemoveMany: ProjectTC.getResolver('removeMany'),
  // Habitat
  habitatCreate: HabitatTC.getResolver('createOne'),
})

const graphqlSchema = GQC.buildSchema()

module.exports = graphqlSchema
