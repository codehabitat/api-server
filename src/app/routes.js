'use strict'

const mainController = require('./controllers/mainController')
const configurationController = require('./controllers/configurationController')
const graphqlHTTP = require('express-graphql')
const authentication = require('./fn/authentication')
const MyGraphQLSchema = require('./models')

module.exports = app => {
  app.get('/', mainController.index)

  app.get('/configuration', configurationController.index)

  app.use(
    '/graphqli',
    graphqlHTTP({
      schema: MyGraphQLSchema,
      graphiql: true,
    })
  )

  app.use(authentication.initialize())
  app.use(authentication.authenticate('google-token'))

  app.post(
    '/graphql',
    graphqlHTTP({
      schema: MyGraphQLSchema,
      graphiql: false,
    })
  )
}
