'use strict'

const mongoose = require('mongoose')

const parameters = require('./../config')
const debug = require('debug')('habitat:mongoose')
mongoose.Promise = require('bluebird')

// Mongoose connection options
const connOptions = {
  // poolSize: 20,
  socketTimeoutMS: 360000,
  // server: {
  // socketOptions: {
  //     keepAlive: 30000,
  //     connectTimeoutMS: 30000,
  //     socketTimeoutMS: 360000,
  // },
  // reconnectTries: Number.MAX_SAFE_INTEGER,
  // },
}
// connOptions.replset = connOptions.server

// Create client connection
const client = (module.exports = createConnection())

module.exports.connOptions = connOptions

function createConnection() {
  return mongoose.createConnection(parameters.mongodbConnectionUri, connOptions)
}

// Define method is connected to test
client.getReadyState = function() {
  return client.readyState
}

// Throw global error if can't open first connection
client.on('error', err => {
  throw err
})

// Debug events
const eventNames = [
  'connecting',
  'connected',
  'open',
  'disconnecting',
  'disconnected',
  'close',
  'reconnected',
  'fullsetup',
  'error',
  'all',
]
eventNames.forEach(function(eventName) {
  client.on(eventName, function(e) {
    if (e) debug(eventName, client.readyState, e.message || e)
    else debug(eventName, client.readyState)
  })
})

if (process.env.NODE_ENV === 'development') {
  mongoose.set('debug', function(collectionName, methodName, ...args) {
    debug('query', collectionName, methodName, ...args)
  })
  // mongoose.set('debug', true)
}
