'use strict'

const passport = require('passport')
const GoogleTokenStrategy = require('passport-google-token').Strategy

const authenticationParams = require('./../../parameters').authentication

const User = require('./../models/user')

passport.use(
  new GoogleTokenStrategy(
    {
      clientID: authenticationParams.googleClientId,
      clientSecret: authenticationParams.googleClientSecret,
    },
    function(accessToken, refreshToken, profile, done) {
      User.findOne({ googleId: profile.id })
        .then(user => {
          if (user) return user
          else
            return User.create({ googleId: profile.id }).then(user => {
              return user
            })
        })
        .then(user => {
          return done(null, user)
        })
        .catch(err => {
          done(err)
        })
    }
  )
)

passport.serializeUser(function(user, done) {
  done(null, user)
})

passport.deserializeUser(function(user, done) {
  done(null, user)
})

module.exports = passport
