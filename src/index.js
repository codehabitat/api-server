'use strict'

const debug = require('debug')('habitat:root')

debug('generate')

const app = require('./app')
const parameters = require('./app/config')

const listenPort = process.env.LISTEN_PORT || parameters.listenPort
const host = process.env.LISTEN_HOST || parameters.listenHost || 'localhost'

app.listen(listenPort, host, () => {
  debug('listening', listenPort)
})

module.exports = 'hello world'
